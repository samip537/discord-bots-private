# Author: samip5

import aiohttp
import discord
import requests
from discord.ext import commands
from plexapi.server import PlexServer

PLEX_TOKEN = ""
PLEX_SRV = ""
PLEX_URL = ""

TAUTULLI_API_KEY = ""
TAUTULLI_BASE_URL = "https://"


class Plex:
    def __init__(self, bot):
        self.bot = bot

    @commands.command(name='streams', description="Returns currently playing streams on Plex.",
                      brief="Display current streams")
    async def query_plex_streams(self, ctx):
        sess = requests.Session()
        sess.verify = False
        server = PlexServer(baseurl=PLEX_SRV, token=PLEX_TOKEN, session=sess)
        session_check = server.sessions() == []
        for session in server.sessions():
            state = session.players[0].state
            player = session.players[0].platform
            duration = session.duration
            duration_millis = session.duration
            duration_minutes = int(duration_millis / (1000 * 60)) % 60
            duration_hours = hours = (duration_millis / (1000 * 60 * 60)) % 24
            if duration_hours >= 1:
                total_duration = f'{duration_hours} Hours and {duration_minutes} Minutes'
            else:
                # total_duration = ("%d Minutes" % duration_minutes)
                total_duration = f'{duration_minutes} Minutes'
            view_offset = session.viewOffset
            view_offset_millis = session.viewOffset
            view_offset_minutes = int(view_offset_millis / (1000 * 60)) % 60
            view_offset_hours = int(view_offset_millis / (1000 * 60 * 60)) % 24
            if view_offset_hours >= 1:
                offset = f'{view_offset_hours} Hours and {view_offset_minutes} Minutes'
            else:
                offset = f'{view_offset_minutes} Minutes'
            percentage = round(view_offset / duration * 100)
            username = session.usernames[0]
            if session.type == 'episode':
                episode_number = int(session.index)
                season = int(session.parentIndex)
                season_and_ep_formatted = ("(s%d:e%d)" % (season, episode_number))
                current_tv_token = f"?checkFiles=1&X-Plex-Token={PLEX_TOKEN}"
                current_tv_thumb = PLEX_URL + session.thumb + current_tv_token
                title = session.grandparentTitle + ' - ' + session.title + ' ' + season_and_ep_formatted
            if session.type == 'movie':
                year = ("(%d)" % session.year)
                current_movie_token = f"?checkFiles=1&X-Plex-Token={PLEX_TOKEN}"
                current_movie_thumb = PLEX_URL + session.thumb + current_movie_token
                # print(current_movie_thumb)
                title = session.title + ' ' + year
            embed = discord.Embed(title="Currently streaming", description="", color=0x00ff00)
            embed.add_field(name="Username", value="{}".format(username))
            embed.add_field(name="Player", value="".join(player), inline=False)
            embed.add_field(name="Title", value="".join(title), inline=False)
            embed.add_field(name="State", value="".join(state), inline=False)
            embed.add_field(name="Watched Duration",
                            value="{watched} ({procent} %)".format(watched=offset, procent=percentage), inline=False)
            embed.add_field(name="Total Duration", value="".join(total_duration), inline=False)
            if session.type == 'episode':
                embed.set_thumbnail(url=current_tv_thumb)
            else:
                embed.set_thumbnail(url=current_movie_thumb)
            embed.set_footer(text="Powered by plexapi.")
            await ctx.say(embed=embed)
        if session_check:
            await ctx.say("Nothing is currently streaming.")

    @commands.command(name='plex-search', description="Search from PLex", brief="Search Plex")
    async def search_plex_though_tautulli(self, ctx, input):
        tautulli_url = "{tautulli_base_url}api/v2?apikey={api_key}&cmd=search&query={string}".format(
            tautulli_base_url=TAUTULLI_BASE_URL, api_key=TAUTULLI_API_KEY, string=input)
        async with aiohttp.ClientSession() as ses:
            async with ses.get(tautulli_url) as resp:
                a = await resp.json()
                for entry in range(a['response']['data']['results_count']):
                    is_TvShow = (a['response']['data']['results_list']['show'])
                    is_movie = (a['response']['data']['results_list']['movie'])
                    if is_TvShow:
                        title = (a['response']['data']['results_list']['show'][entry]['title'])
                        year = (a['response']['data']['results_list']['show'][entry]['year'])
                        desc = (a['response']['data']['results_list']['show'][entry]['summary'])
                        thumbail_not_full = (a['response']['data']['results_list']['show'][entry]['thumb'])
                        tv_plex_token = '?checkFiles=1&X-Plex-Token=' + PLEX_TOKEN
                        tv_generated_url = PLEX_URL + thumbail_not_full + tv_plex_token
                        tv_embed = discord.Embed(title="Search results for TV", description="", color=0x00ff00)
                        tv_embed.set_thumbnail(url=tv_generated_url)
                        tv_embed.add_field(name="Title", value=title)
                        tv_embed.add_field(name="Year", value=year)
                        tv_embed.add_field(name="Summary", value=desc)
                        await ctx.say(embed=tv_embed)
                    elif is_movie:
                        movie_title = (a['response']['data']['results_list']['movie'][entry]['title'])
                        movie_genre = (a['response']['data']['results_list']['movie'][entry]['genres'])
                        movie_year = (a['response']['data']['results_list']['movie'][entry]['year'])
                        movie_desc = (a['response']['data']['results_list']['movie'][entry]['summary'])
                        movie_thumb_not_full = (a['response']['data']['results_list']['movie'][entry]['thumb'])
                        movie_plex_token = '?checkFiles=1&X-Plex-Token=' + PLEX_TOKEN
                        movie_generated_url = PLEX_URL + movie_thumb_not_full + movie_plex_token
                        movie_embed = discord.Embed(title="Search results for Movies", description="", color=0x00ff00)
                        movie_embed.set_thumbnail(url=movie_generated_url)
                        movie_embed.add_field(name="Title", value=movie_title)
                        movie_embed.add_field(name="Genre", value=" ,\n".join(movie_genre))
                        movie_embed.add_field(name="Year", value=movie_year)
                        movie_embed.add_field(name="Summary", value=movie_desc)
                        await ctx.say(embed=movie_embed)
                    else:
                        await ctx.say("Unable to find anything.")
                        break


def setup(bot):
    bot.add_cog(Plex(bot))
    print("Plex extension has been loaded.")
