import discord
from discord.ext import commands


class General:
	def __init__(self, bot):
		self.bot = bot

	@commands.command(name="ping", brief="Ping pong")
	async def ping(self, ctx):
		await ctx.send("Pong!")


def setup(bot):
	bot.add_cog(General(bot))
