import random
import aiohttp
import datetime
import discord
from discord.ext import commands


class Misc:
	def __init__(self, bot):
		self.bot = bot

	@commands.command(name='8ball', description="Answers a yes/no question.", brief="Answers from the beyond.", pass_context=True)
	async def answer_machine(self, ctx):
		responses = [
			'That is a resounding no',
			'It is not looking likely',
			'Too hard to tell',
			'It is quite possible',
			'Definitely', ]
		await ctx.say(random.choice(responses) + ", " + ctx.message.author.mention)

	@commands.command(name='facepalmed', brief="Command says everything needed.", pass_context=True)
	async def facepalm(self, ctx):
		urls = [
			'https://media1.giphy.com/media/AjYsTtVxEEBPO/giphy.gif',
			'https://media2.giphy.com/media/3og0INyCmHlNylks9O/giphy.gif',
			'https://media2.giphy.com/media/ADr35Z4TvATIc/giphy.gif',
			'https://media3.giphy.com/media/6yRVg0HWzgS88/giphy.gif']

		thumb_url = random.choice(urls)
		f = discord.Embed(color=random.randint(1, 255 ** 3 - 1))
		f.set_image(url=thumb_url)
		f.set_footer(text="Powered by Giphy.")
		await ctx.send(embed=f)

	@commands.command(name='cat', description="Gives you a random cat picture", brief="Returns random image of a cat")
	async def randomcat(self, ctx):
		async with aiohttp.ClientSession() as ses:
			async with ses.get('https://aws.random.cat/meow') as response:
				ret = await response.json()
		e = discord.Embed(color=random.randint(1, 255 ** 3 - 1))
		e.set_image(url=ret['file'])
		e.set_author(name="Random.cat", url='https://random.cat/')
		e.set_footer(text="Powered by random.cat")
		await ctx.send(embed=e)

	async def on_member_update(self, before, after):
		if before.nick != after.nick:
			if after.nick != before.display_name:
				await self.bot.db.execute('''
					CREATE TABLE IF NOT EXISTS discord_users_list(
						id int PRIMARY,
						userID int NOT NULL,
						displayName str NOT NULL,
						before_nick str NOT NULL,
						after_nick str NOT NULL,
						time date
					);''')
				date = datetime.datetime.now()
				await self.bot.db.execute(f"INSERT INTO discord_users_list(userID, displayName, before_nick, "
										  f"after_nick) values ({before.id}, {before.display_name} ,{before.nick}, "
										  f"{after.nick}, {date})")
				await self.bot.db.commit()

		pass
		# print(f'Before nick: {before.nick}')
		# print(f'Before display_name: {before.display_name}')
		# print(f'Before id: {before.id}')
		# print(f'After nick: {after.nick}')
		# print(f'After display_name: {after.display_name}')
		# print(f'After id: {after.id}')


def setup(bot):
	bot.add_cog(Misc(bot))
