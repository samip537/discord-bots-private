import configparser
import datetime
import sys
import traceback

import discord
from discord.ext import commands


def get_prefix(bot, message):
    prefixes = ['?']
    return commands.when_mentioned_or(*prefixes)(bot, message)


initial_extensions = ['cogs.meta', 'cogs.owner', 'cogs.music', 'cogs.error_handler', 'jishaku']

bot = commands.Bot(command_prefix=get_prefix, description="This is a multi-purpose bot")

config = configparser.ConfigParser()
config.read("tokens.ini")
dev_token = config.get('token', 'dev')
production_token = config.get('token', 'prod')


@bot.event
async def on_ready():
    bot.uptime = datetime.datetime.utcnow()
    print(f'\n\nLogged in as: {bot.user.name} - {bot.user.id}\nAPI Version: {discord.__version__}\n')
    print(f'Server count: {len(bot.guilds)}')


if __name__ == '__main__':
    if any('debug' in arg.lower() for arg in sys.argv):
        bot.command_prefix = '$'
    for extension in initial_extensions:
        try:
            bot.load_extension(extension)
        except:
            print(f'Failed to load extension {extension}.', file=sys.stderr)
            traceback.print_exc()
# bot.run(dev_token)

bot.run(production_token)
