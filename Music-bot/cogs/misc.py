import discord
from discord.ext import commands


class Misc:
	def __init__(self, bot):
		self.bot = bot

	@commands.command(name='top_role', aliases=['toprole'], pass_context=True)
	async def show_toprole(self, ctx, *, member: discord.Member = None):
		"""Simple command which shows the members Top Role."""

		if member is None:
			member = ctx.message.author

		await ctx.send(f'The top role for {member.display_name} is {member.top_role.name}')


def setup(bot):
	bot.add_cog(Misc(bot))
