import discord
import datetime
import asyncio
import os
import socket
from discord.ext import commands


class Meta:
    """Commands for utilities related to Discord or the Bot itself."""

    def __init__(self, bot):
        self.bot = bot

    def get_bot_uptime(self):
        now = datetime.datetime.utcnow()
        delta = now - self.bot.uptime
        hours, remainder = divmod(int(delta.total_seconds()), 3600)
        minutes, seconds = divmod(remainder, 60)
        days, hours = divmod(hours, 24)
        if days:
            fmt = '{d} days, {h} hours and {m} minutes'
        else:
            fmt = '{h} hours and {m} minutes'
        return fmt.format(d=days, h=hours, m=minutes, s=seconds)

    def get_system_uptime(self):
        if os.name == 'nt':
            return "Running on Windows, unable to figure out."

        try:
            f = open("/proc/uptime")
            contents = f.read().split()
            f.close()
        except:
            return "Cannot open uptime file: /proc/uptime"

        total_seconds = float(contents[0])

        # Helper vars:
        MINUTE = 60
        HOUR = MINUTE * 60
        DAY = HOUR * 24

        # Get the days, hours, etc:
        days = int(total_seconds / DAY)
        hours = int((total_seconds % DAY) / HOUR)
        minutes = int((total_seconds % HOUR) / MINUTE)
        seconds = int(total_seconds % MINUTE)

        # Build up the pretty string (like this: "N days, N hours, N minutes, N seconds")
        string = ""
        if days > 0:
            string += str(days) + " " + (days == 1 and "day" or "days") + ", "
        if len(string) > 0 or hours > 0:
            string += str(hours) + " " + (hours == 1 and "hour" or "hours") + ", "
        if len(string) > 0 or minutes > 0:
            string += str(minutes) + " " + (minutes == 1 and "minute" or "minutes")

        return string

    @commands.command()
    async def system_uptime(self, ctx):
        """Tells you how long the system has been up for."""
        await ctx.send('System uptime: **{}**'.format(self.get_system_uptime()))

    @commands.command()
    async def uptime(self, ctx):
        """Tells you how long the bot has been up for."""
        await ctx.send('Uptime: **{}**'.format(self.get_bot_uptime()))

    @commands.command(name='quit', hidden=True)
    @commands.is_owner()
    async def _quit(self, ctx):
        """Quits the bot."""
        channel = ctx.channel
        await ctx.send(f'Please confirm the action, the bot will quit entirely.'
                       f'You need to confirm it by saying just "confirm".')

        def check(m):
            return m.content == 'confirm' and m.channel == channel

        try:
            msg = await self.bot.wait_for('message', timeout=30.0, check=check)
        except asyncio.TimeoutError:
            return await ctx.send('You took long. Goodbye.')

        await ctx.send(f'Confirmed, exiting...')
        await self.bot.logout()

    @commands.command(name="about")
    async def about_me(self, ctx):
        """Tells you information about the bot itself."""
        result = ['**About Me:**']
        result.append('- Author: samip537 (Discord ID: 157970669261422592, Github: samip5)')
        result.append(f'- Bot ID: {self.bot.user.name} (Discord ID: {self.bot.user.id})')
        result.append('- Created on July 3rd, 2018')
        result.append('- Library: discord.py (Python)')
        result.append(f'- System hostname: {socket.gethostname()}')
        # changes = os.popen(
        #	r'git show -s HEAD~3..HEAD --format="[%h](https://github.com/samip5/Discord-Bots/commit/%H) %s (%cr)"').read().strip()
        # result.append('- Changes: {}'.format(changes))
        result.append('- Bot uptime: {}'.format(self.get_bot_uptime()))
        result.append('- System uptime: {}'.format(self.get_system_uptime()))
        result.append('- Servers: {}'.format(len(self.bot.guilds)))
        # stats
        total_members = sum(len(s.members) for s in self.bot.guilds)
        total_online = sum(1 for m in self.bot.get_all_members() if m.status != discord.Status.offline)
        unique_members = set(self.bot.get_all_members())
        unique_online = sum(1 for m in unique_members if m.status != discord.Status.offline)
        result.append('- Total Members: {} ({} online)'.format(total_members, total_online))
        result.append('- Unique Members: {} ({} online)'.format(len(unique_members), unique_online))
        await ctx.send('\n'.join(result))


def setup(bot):
    bot.add_cog(Meta(bot))
