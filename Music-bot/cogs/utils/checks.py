import discord
from discord.ext import commands


async def check_permissions(ctx, perms, *, check=all):
    is_owner = await ctx.bot.is_owner(ctx.author)
    if is_owner:
        return True

    resolved = ctx.channel.permissions_for(ctx.author)
    return check(getattr(resolved, name, None) == value for name, value in perms.items())


def has_permissions(*, check=all, **perms):
    async def pred(ctx):
        return await check_permissions(ctx, perms, check=check)
    return commands.check(pred)


async def check_guild_permissions(ctx, perms, *, check=all):
    is_owner = await ctx.bot.is_owner(ctx.author)
    if is_owner:
        return True

    if ctx.guild is None:
        return False

    resolved = ctx.author.guild_permissions
    return check(getattr(resolved, name, None) == value for name, value in perms.items())


def has_guild_permissions(*, check=all, **perms):
    async def pred(ctx):
        return await check_guild_permissions(ctx, perms, check=check)
    return commands.check(pred)


def is_in_guilds(*guild_ids):
    def predicate(ctx):
        guild = ctx.guild
        if guild is None:
            return False
        return guild.id in guild_ids
    return commands.check(predicate)


def am_i_owner():
    async def predicate(ctx):
        is_owner = await ctx.bot.is_owner(ctx.author)
        if is_owner:
            return True
        else:
            return False
    return commands.check(predicate)


def is_mod():
    async def pred(ctx):
        return await check_guild_permissions(ctx, {'manage_guild': True})
    return commands.check(pred)


def is_admin():
    async def pred(ctx):
        return await check_guild_permissions(ctx, {'administrator': True})
    return commands.check(pred)


def mod_or_permissions(**perms):
    perms['manage_guild'] = True

    async def predicate(ctx):
        return await check_guild_permissions(ctx, perms, check=any)
    return commands.check(predicate)


def admin_or_permissions(**perms):
    perms['administrator'] = True

    async def predicate(ctx):
        return await check_guild_permissions(ctx, perms, check=any)
    return commands.check(predicate)


def song_requester_or_dj():
    async def predicate(ctx):
        vc = ctx.voice_client
        try:
            if ctx.message.author.name == vc.source.requester.name:
                return True
            elif discord.utils.get(ctx.message.author.roles, name="DJ"):
                return True
            elif ctx.message.author != vc.source.requester or not discord.utils.get(ctx.message.author.roles, name="DJ"):
                await ctx.send(f"The bot owner has been naughty and has failed to add a voting system, so sorry but your "
                               f"permissions are not enough to use this command.")
                return False
        except AttributeError:
            pass

    return commands.check(predicate)


def song_requester_or_owner_or_dj():
    async def predicate(ctx):
        is_owner = await ctx.bot.is_owner(ctx.author)
        try:
            vc = ctx.voice_client

            if ctx.message.author.name == vc.source.requester.name:
                return True
            elif is_owner:
                return True
            elif discord.utils.get(ctx.message.author.roles, name="DJ"):
                return True
            elif ctx.message.author != vc.source.requester or not discord.utils.get(ctx.message.author.roles, name="DJ"):
                await ctx.send(f"The bot owner has been naughty and has failed to add a voting system, so sorry but your "
                           f"permissions are not enough to use this command.")
                return False
        except AttributeError:
            pass
    return commands.check(predicate)


def music_stop_check():
    async def predicate(ctx):
        is_owner = await ctx.bot.is_owner(ctx.author)
        vc = ctx.voice_client
        if is_owner:
            return True
        elif discord.utils.get(ctx.message.author.roles, name="DJ"):
            return True
        elif ctx.message.author != vc.source.requester or not discord.utils.get(ctx.message.author.roles, name="DJ"):
            await ctx.send(f"You lack the permissions to use this command, sorry.")
            return False
    return commands.check(predicate)